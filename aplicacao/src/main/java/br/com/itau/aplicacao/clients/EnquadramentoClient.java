package br.com.itau.aplicacao.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.itau.aplicacao.models.Aplicacao;


@FeignClient(name="enquadramento")
public interface EnquadramentoClient {
	
	@GetMapping("/enquadramento/{idCliente}")
	public boolean verificarEnquadramento
	(@PathVariable("idCliente") Integer idCliente, @RequestBody Aplicacao aplicacao );

}


