package br.com.itau.aplicacao.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.aplicacao.clients.ClienteClient;
import br.com.itau.aplicacao.clients.EnquadramentoClient;
import br.com.itau.aplicacao.clients.PerfilInvestidorClient;
import br.com.itau.aplicacao.clients.ProdutoClient;
import br.com.itau.aplicacao.clients.TermoClienteClient;
import br.com.itau.aplicacao.models.Aplicacao;
import br.com.itau.aplicacao.repositories.AplicacaoRepository;
import br.com.itau.aplicacao.viewobjects.AplicacaoSaida;
import br.com.itau.aplicacao.viewobjects.ClienteTermo;
import br.com.itau.aplicacao.viewobjects.PerfilInvestidor;
import br.com.itau.aplicacao.viewobjects.Produto;

@Service
public class AplicacaoService {
	
	@Autowired
	AplicacaoRepository aplicacaoRepository;
	
	@Autowired
	ClienteClient clienteClient;
	
	@Autowired
	ProdutoClient produtoClient;
	
	@Autowired
	EnquadramentoClient enquadramentoClient;
	
	@Autowired
	TermoClienteClient termoClienteClient;
	
	@Autowired
	PerfilInvestidorClient perfilInvestidorClient;
	
	
	public Iterable<AplicacaoSaida> listarAplicacoes(int idCliente){
		
		List<AplicacaoSaida> listasaida = new ArrayList<AplicacaoSaida>();

//		ClienteSaida cliente = clienteClient.buscarCliente(idCliente);

		Iterable<Aplicacao> listaaplicacao = aplicacaoRepository.findAllByidCliente(idCliente);
		
		for (Aplicacao aplicacao : listaaplicacao) {
			AplicacaoSaida aplicacaoSaida = new AplicacaoSaida();
			aplicacaoSaida.setIdAplicacao(aplicacao.getIdAplicacao());
			aplicacaoSaida.setMes(aplicacao.getMeses());
			aplicacaoSaida.setIdProduto(aplicacao.getIdProduto());
			aplicacaoSaida.setValor(aplicacao.getValor());
			listasaida.add(aplicacaoSaida);
			
		}
		return listasaida;
	}
	
	public int calcular(Aplicacao aplicacao){
		
		Optional<PerfilInvestidor> perfilInvestidorOptional = perfilInvestidorClient.obterPerfilInvestidor(aplicacao.getIdCliente());
		
		if (!perfilInvestidorOptional.isPresent()) {
			return verificarTCQCliente(aplicacao.getIdCliente(), aplicacao );
		 }
			
		Optional<Produto> produtoOptional = produtoClient.buscarProdutoId(aplicacao.getIdProduto());
		
		if(!produtoOptional.isPresent()) {
			return 3;
		}

		if(enquadramentoClient.verificarEnquadramento(aplicacao.getIdCliente(), aplicacao)) {
			aplicacaoRepository.save(aplicacao);
			return 0;
		}else{
			return 2;
		}
		

	}
	
	private int verificarTCQCliente(int idCliente, Aplicacao aplicacao) {
		int termo = 1;
		ClienteTermo clienteTermo = new ClienteTermo();
		clienteTermo.setIdCliente(idCliente);
		clienteTermo.setIdTermo(termo);
		if(!termoClienteClient.buscarClienteTermo(idCliente, termo)) {
			return 1;
		}else {
			aplicacaoRepository.save(aplicacao);
			return 0;
		}	 
	}
}
