package br.com.itau.aplicacao.viewobjects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


public class Produto {

	private int idProduto;
	private double rendimento;
	private String nomeProduto;
	private int riscoProduto;
	
	public int getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}
	public double getRendimento() {
		return rendimento;
	}
	public void setRendimento(double rendimento) {
		this.rendimento = rendimento;
	}
	public String getNomeProduto() {
		return nomeProduto;
	}
	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}
	public int getRiscoProduto() {
		return riscoProduto;
	}
	public void setRiscoProduto(int riscoProduto) {
		this.riscoProduto = riscoProduto;
	}
}
