package br.com.itau.aplicacao.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.itau.aplicacao.viewobjects.ClienteTermo;


@FeignClient(name="termo")
public interface TermoClienteClient {
	
	@GetMapping("termo/buscar/{idCliente}/{idTermo}")
	public boolean buscarClienteTermo(@PathVariable("idCliente") int idCliente, @PathVariable("idTermo") int termo );

}


