package br.com.itau.aplicacao.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.aplicacao.viewobjects.ClienteSaida;

@FeignClient(name="cliente")
public interface ClienteClient {
	
	@GetMapping("/cliente/{idCliente}")
	public ClienteSaida buscarCliente(@PathVariable("idCliente") int idCliente);

}


