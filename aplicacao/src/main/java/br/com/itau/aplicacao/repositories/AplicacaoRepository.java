package br.com.itau.aplicacao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.aplicacao.models.Aplicacao;


public interface AplicacaoRepository extends JpaRepository<Aplicacao, Integer> {

	public Iterable<Aplicacao> findAllByidCliente(int idCliente);
}
