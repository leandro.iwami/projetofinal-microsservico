package br.com.itau.enquadramento.clients;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.enquadramento.viewobjects.PerfilInvestidor;


@FeignClient(name="perfilinvestidor")
public interface PerfilInvestidorClient {
	
	@GetMapping("/perfilinvestidor/{idCliente}")
	public Optional<PerfilInvestidor> obterPerfilInvestidor(@PathVariable("idCliente") Integer idCliente);

}


