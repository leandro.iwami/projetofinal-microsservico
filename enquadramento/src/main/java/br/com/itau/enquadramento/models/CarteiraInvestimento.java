package br.com.itau.enquadramento.models;

import java.util.List;

import br.com.itau.enquadramento.viewobjects.Aplicacao;

public class CarteiraInvestimento {
	
	private int idCliente;
	private List<Aplicacao> aplicacao;
	
	
	public List<Aplicacao> getAplicacao() {
		return aplicacao;
	}
	public void setAplicacao(List<Aplicacao> aplicacao) {
		this.aplicacao = aplicacao;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	
	

}
