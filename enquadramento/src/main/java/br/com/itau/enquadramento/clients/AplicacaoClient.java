package br.com.itau.enquadramento.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.enquadramento.viewobjects.AplicacaoSaida;


@FeignClient(name="aplicacao")
public interface AplicacaoClient {
	
	@GetMapping("aplicacao/{idCliente}/listar")
	public Iterable<AplicacaoSaida> listarAplicacoes(@PathVariable("idCliente") int idCliente);

}


