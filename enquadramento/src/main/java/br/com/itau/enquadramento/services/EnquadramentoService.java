package br.com.itau.enquadramento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.enquadramento.clients.AplicacaoClient;
import br.com.itau.enquadramento.clients.PerfilInvestidorClient;
import br.com.itau.enquadramento.clients.ProdutoClient;
import br.com.itau.enquadramento.clients.TermoClienteClient;
import br.com.itau.enquadramento.viewobjects.Aplicacao;
import br.com.itau.enquadramento.viewobjects.AplicacaoSaida;
import br.com.itau.enquadramento.viewobjects.ClienteTermo;
import br.com.itau.enquadramento.viewobjects.PerfilInvestidor;
import br.com.itau.enquadramento.viewobjects.Produto;

@Service
public class EnquadramentoService {

	@Autowired
	AplicacaoClient aplicacaoClient;

	@Autowired
	ProdutoClient produtoClient;

	@Autowired
	EnquadramentoService carteiraInvestimentoService;

	@Autowired
	PerfilInvestidorClient perfilInvestidorClient;

	@Autowired
	TermoClienteClient termoClienteClient;

	private int calcularCarteiraInvestimento(int idCliente, double valor, int idProduto) {

		double valorTotal = 0;
		double valorTotalRisco = 0;
		double valorFinal = 0;

		Produto produto = new Produto();

		Optional<Produto> produtoOptional = produtoClient.buscarProdutoId(idProduto);
		
		if (!produtoOptional.isPresent()) {
			return 0;
		}
		
		produto = produtoOptional.get();
		
		valorTotalRisco += valor * produto.getRiscoProduto();
		valorTotal += valor;

		Iterable<AplicacaoSaida> aplicacoes = aplicacaoClient.listarAplicacoes(idCliente);
		for (AplicacaoSaida aplicacaoSaida : aplicacoes) {

			produtoOptional = produtoClient.buscarProdutoId(aplicacaoSaida.getIdProduto());
			
			if (!produtoOptional.isPresent()) {
				return 0;
			}
			produto = produtoOptional.get();
			valorTotalRisco += aplicacaoSaida.getValor() * produto.getRiscoProduto();
			valorTotal += aplicacaoSaida.getValor();

		}
		valorFinal = valorTotalRisco / valorTotal;

		return buscarFaixaPerfil(valorFinal);

	}

	private int buscarFaixaPerfil(double valor) {
		if (0 < valor && valor <= 5) {
			return 1;
		} else {
			if (5.01 < valor && valor <= 20) {
				return 2;
			} else {
				if (20.01 < valor && valor <= 35) {
					return 3;
				} else {
					return 4;
				}
			}
		}

	}

	public boolean verificarEnquadramento( int idCliente, Aplicacao aplicacao) {
		int perfilCarteira = 0;
		int perfilCliente = 0;
		int termo = 2;
		
		ClienteTermo clienteTermo = new ClienteTermo();
		clienteTermo.setIdCliente(idCliente);
		clienteTermo.setIdTermo(termo);

		if (termoClienteClient.buscarClienteTermo(idCliente, termo)) {
			return true;
		}

		perfilCarteira = carteiraInvestimentoService.calcularCarteiraInvestimento( idCliente, aplicacao.getValor(), aplicacao.getIdProduto());
		
		Optional<PerfilInvestidor> perfilClienteOptional = perfilInvestidorClient.obterPerfilInvestidor(idCliente);

		if (!perfilClienteOptional.isPresent()) {
			perfilCliente = 0;
		}

		if (perfilCarteira <= perfilCliente) {
			return true;
		} else {
			return false;
		}

	}
}
