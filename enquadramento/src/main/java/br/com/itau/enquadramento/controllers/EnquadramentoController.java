package br.com.itau.enquadramento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.enquadramento.services.EnquadramentoService;
import br.com.itau.enquadramento.viewobjects.Aplicacao;

@RestController
@RequestMapping("/enquadramento")
public class EnquadramentoController {
	
	@Autowired
	EnquadramentoService enquadramentoService;
	
	@GetMapping("/{idCliente}")
	public boolean verificarEnquadramento
	(@PathVariable Integer idCliente, @RequestBody Aplicacao aplicacao ) {
		return enquadramentoService.verificarEnquadramento(idCliente, aplicacao);
	}


}
