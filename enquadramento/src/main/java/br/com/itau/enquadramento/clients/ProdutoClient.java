package br.com.itau.enquadramento.clients;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.enquadramento.viewobjects.Produto;


@FeignClient(name="produto")
public interface ProdutoClient {
	
	@GetMapping("/produto/{idProduto}")
	public Optional<Produto> buscarProdutoId(@PathVariable("idProduto") Integer idProduto);

}


