package br.com.itau.enquadramento.viewobjects;

import java.time.LocalDate;


public class PerfilInvestidor {
	
	
	private Integer idCliente;
	
	private Integer codigoPerfil;
	
	private LocalDate dataAtualizacao;

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public int getCodigoPerfil() {
		return codigoPerfil;
	}

	public void setCodigoPerfil(int codigoPerfil) {
		this.codigoPerfil = codigoPerfil;
	}

	public LocalDate getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(LocalDate dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}
	

	
	
}
