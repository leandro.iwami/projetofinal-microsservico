package br.com.itau.enquadramento.viewobjects;


import java.time.LocalDate;

public class ClienteTermo {
	
	private int idTermoAplicacao;


	private int idCliente;
	

	private int idTermo;


	private LocalDate dataTermo;

	public int getIdTermoAplicacao() {
		return idTermoAplicacao;
	}

	public void setIdTermoAplicacao(int idTermoAplicacao) {
		this.idTermoAplicacao = idTermoAplicacao;
	}

	
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}



	public int getIdTermo() {
		return idTermo;
	}

	public void setIdTermo(int idTermo) {
		this.idTermo = idTermo;
	}

	public LocalDate getDataTermo() {
		return dataTermo;
	}

	public void setDataTermo(LocalDate dataTermo) {
		this.dataTermo = dataTermo;
	}

	
}
