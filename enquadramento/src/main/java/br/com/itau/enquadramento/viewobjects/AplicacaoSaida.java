package br.com.itau.enquadramento.viewobjects;

public class AplicacaoSaida {
	
	private int idAplicacao;
	private int idProduto;
	private double valor;
	private double mes;
	
	public int getIdAplicacao() {
		return idAplicacao;
	}
	public void setIdAplicacao(int idAplicacao) {
		this.idAplicacao = idAplicacao;
	}
	

	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public double getMes() {
		return mes;
	}
	public void setMes(double mes) {
		this.mes = mes;
	}
	public int getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}
	
	

}
