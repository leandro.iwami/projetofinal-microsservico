package br.com.itau.cliente.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.cliente.models.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	public Optional<Cliente> findByCpf(String Cpf);
}
