package br.com.itau.cliente.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.repositories.ClienteRepository;
import br.com.itau.cliente.viewobjects.ClienteSaida;

@Service
public class ClienteService {
	@Autowired
	ClienteRepository clienteRepository;
	
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public ClienteSaida obterClientePorIdSaida(int id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);
		
		Cliente cliente = clienteOptional.get();
		ClienteSaida clientesaida = new ClienteSaida();
		
		clientesaida.setCpf(cliente.getCpf());
		clientesaida.setNome(cliente.getNome());
		
		return clientesaida;
	}
	
	public Cliente obterClientePorId(int id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);
		Cliente cliente = new Cliente(); 
		cliente = clienteOptional.get();	
		return cliente;
	}

	public boolean inserir(Cliente cliente) {
		String hash = encoder.encode(cliente.getSenha());
		cliente.setSenha(hash);

		clienteRepository.save(cliente);
		
		return true;
	}

	public boolean atualizar(Cliente cliente, int id) {
		cliente.setIdCliente(id);
		Optional<Cliente> clienteOptional = clienteRepository.findById(cliente.getIdCliente());

		if (clienteOptional.isPresent()) {
			cliente = mesclarAtributos(cliente, clienteOptional.get());
			clienteRepository.save(cliente);
			return true;
		}
		return false;
	}

	private Cliente mesclarAtributos(Cliente novo, Cliente antigo) {
		
		if (novo.getNome() != null && !novo.getNome().isEmpty()) {
			antigo.setNome(novo.getNome());
		}
				
		if (!novo.getCpf().isEmpty()) {
			antigo.setCpf(novo.getCpf());
		}
		

		return antigo;
	}
	
	public Optional<Cliente> buscarPorCpfESenha(String cpf, String senha) {
		Optional<Cliente> clienteOptional = clienteRepository.findByCpf(cpf);
		
		if(clienteOptional.isPresent()) {
			Cliente cliente = clienteOptional.get();
			
			if(encoder.matches(senha, cliente.getSenha())) {
				return clienteOptional;
			}
		}
		
		return Optional.empty();
	}


}
