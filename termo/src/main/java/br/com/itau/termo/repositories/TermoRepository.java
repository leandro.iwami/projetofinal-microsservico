package br.com.itau.termo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.termo.models.Termo;

public interface TermoRepository extends JpaRepository<Termo, Integer> {

}
