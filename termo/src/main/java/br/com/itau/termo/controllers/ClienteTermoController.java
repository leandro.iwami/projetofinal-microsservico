package br.com.itau.termo.controllers;

import javax.persistence.IdClass;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.termo.models.ClienteTermo;
import br.com.itau.termo.models.Termo;
import br.com.itau.termo.services.ClienteTermoService;
import br.com.itau.termo.services.TermoService;

@RestController
@RequestMapping("/termo")
public class ClienteTermoController {
	@Autowired
	TermoService termoService;
	
	@Autowired
	ClienteTermoService clienteTermoService;
	
	@GetMapping("/buscar/{idCliente}/{idTermo}")
	public boolean buscarClienteTermo(@PathVariable("idCliente") int idCliente, @PathVariable("idTermo") int termo ){
		return clienteTermoService.buscarClienteTermo(idCliente, termo);
	}
	
	@PostMapping("/inserirtermo")
	public void inserirTermo(@RequestBody Termo termo) {
		termoService.inserir(termo);
	}
	@PostMapping("/inserirclientetermo")
	public void inserirClienteTermo(@RequestBody ClienteTermo clientetermo) {
		clienteTermoService.inserir(clientetermo);
	}
	
//	@GetMapping("/{id}/{idT}/teste")
//	public Iterable<ClienteTermo> listarTermos(@PathVariable int id, @PathVariable int idT){
//		return clienteTermoService.buscarClienteTermo(id, idT);
//	}
}
