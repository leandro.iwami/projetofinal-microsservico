package br.com.itau.termo.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.termo.models.ClienteTermo;
import br.com.itau.termo.repositories.ClienteTermoRepository;


@Service
public class ClienteTermoService {
	@Autowired
	ClienteTermoRepository clienteTermoRepository;
	
	public void inserir(ClienteTermo clienteTermo) {
		clienteTermoRepository.save(clienteTermo);		
	}
	
	public Iterable<ClienteTermo> obterClienteTermo(){
		return clienteTermoRepository.findAll();
	}
	
	public Optional<ClienteTermo> obterClienteTermoPorId(int id) {
		return clienteTermoRepository.findById(id);
	}
	
	public boolean buscarClienteTermo(int idCliente, int termo) {

		Iterable<ClienteTermo> listaTermo = clienteTermoRepository.findAllByidCliente(idCliente);

		for (ClienteTermo clienteTermo : listaTermo) {
	
			
			if (clienteTermo.getIdTermo() == termo) {
			return true;
			}
		}
		return false;
	}
}
