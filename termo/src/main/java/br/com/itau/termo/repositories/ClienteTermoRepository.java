package br.com.itau.termo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.termo.models.ClienteTermo;
import br.com.itau.termo.viewmodels.Cliente;

public interface ClienteTermoRepository extends JpaRepository<ClienteTermo, Integer> {
	public Iterable<ClienteTermo> findAllByidCliente(int idCliente);
}
