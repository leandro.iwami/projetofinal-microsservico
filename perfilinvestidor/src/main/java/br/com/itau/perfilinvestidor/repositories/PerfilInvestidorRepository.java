package br.com.itau.perfilinvestidor.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.perfilinvestidor.models.PerfilInvestidor;

public interface PerfilInvestidorRepository extends CrudRepository<PerfilInvestidor, Integer>{

}
