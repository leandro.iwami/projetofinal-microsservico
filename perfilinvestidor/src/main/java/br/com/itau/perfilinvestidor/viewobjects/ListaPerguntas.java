package br.com.itau.perfilinvestidor.viewobjects;

import java.util.ArrayList;
import java.util.List;

import br.com.itau.perfilinvestidor.models.Questionario;

public class ListaPerguntas {

	private List<Questionario> listaPerguntas = new ArrayList<Questionario>();

	public List<Questionario> getListaPerguntas() {
		return listaPerguntas;
	}

	public void setListaPerguntas(List<Questionario> listaPerguntas) {
		this.listaPerguntas = listaPerguntas;
	}

}
