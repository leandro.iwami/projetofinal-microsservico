package br.com.itau.perfilinvestidor.services;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.perfilinvestidor.models.PerfilInvestidor;
import br.com.itau.perfilinvestidor.repositories.PerfilInvestidorRepository;

@Service

public class PerfilInvestidorService {

	@Autowired
	PerfilInvestidorRepository perfilInvestidorRepository;

	public PerfilInvestidor alterarPerfil(Integer idCliente, Integer codigoPerfil) {

		PerfilInvestidor perfil = new PerfilInvestidor();

		Optional<PerfilInvestidor> perfilExiste = consultarPerfilInvestidor(idCliente);

		if (perfilExiste.isPresent()) {
			perfil = perfilExiste.get();
			perfil.setCodigoPerfil(codigoPerfil);
			perfil.setDataAtualizacao(LocalDate.now());
			perfilInvestidorRepository.save(perfil);
			return perfil;
		}
		
		perfil.setIdCliente(idCliente);
		perfil.setCodigoPerfil(codigoPerfil);
		perfil.setDataAtualizacao(LocalDate.now());
		perfilInvestidorRepository.save(perfil);
		return perfil;
	}

	public Optional<PerfilInvestidor> consultarPerfilInvestidor(Integer idCliente) {

		Optional<PerfilInvestidor> perfil = perfilInvestidorRepository.findById(idCliente);

		if (!perfil.isPresent()) {
			return Optional.empty();
		}

		return perfil;

	}
}
