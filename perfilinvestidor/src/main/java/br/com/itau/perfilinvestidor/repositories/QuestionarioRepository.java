package br.com.itau.perfilinvestidor.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.perfilinvestidor.models.Questionario;


public interface QuestionarioRepository extends JpaRepository<Questionario, Integer> {

}
