package br.com.itau.perfilinvestidor.viewobjects;

public class Resposta {
	private int resposta;

	public int getResposta() {
		return resposta;
	}

	public void setResposta(int resposta) {
		this.resposta = resposta;
	}

}
