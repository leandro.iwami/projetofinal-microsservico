package br.com.itau.produto.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.produto.models.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
