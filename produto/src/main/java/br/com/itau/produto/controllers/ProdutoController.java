package br.com.itau.produto.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.produto.models.Produto;
import br.com.itau.produto.services.ProdutoService;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
	
	@Autowired
	ProdutoService produtoService;
	
	@GetMapping
	public Iterable<Produto> buscarProduto(){
		return produtoService.obterProdutos();
	}
	
	@GetMapping("/{id}")
	public Optional<Produto> buscarProdutoId(@PathVariable int id){
		return produtoService.obterProdutoPorId(id);
	}
	
	@PostMapping("/inserir")
	public void inserirProduto(@RequestBody Produto produto) {
		produtoService.inserir(produto);
	}
	@PatchMapping("/{id}")
	public ResponseEntity atualizarProduto(@PathVariable int id, @RequestBody Produto produto) {
		boolean resultado = produtoService.atualizar(id, produto);

		if (!resultado) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(produto);
	}

}
